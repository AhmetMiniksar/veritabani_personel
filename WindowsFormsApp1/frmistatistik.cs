﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace WindowsFormsApp1
{
    public partial class frmistatistik : Form
    {
        public frmistatistik()
        {
            InitializeComponent();
        }

        SqlConnection baglanti = new SqlConnection("Data Source=AHMETM91;Initial Catalog=PersonelVeriTabani;Integrated Security=True");
        private void frmistatistik_Load(object sender, EventArgs e)
        {
            toplamPersonel();
            evliPersonel();
            bekarPersonel();
            sehirSayisi();
            toplamMaas();
            ortalamaMaas();
        }

        public void toplamPersonel()
        {
            baglanti.Open();
            SqlCommand komut1 = new SqlCommand("select count(*) from per_table", baglanti);
            SqlDataReader d1 = komut1.ExecuteReader();
            while (d1.Read())
            {
                lbl_toplampersonel.Text = d1[0].ToString();
            }
            baglanti.Close();
        }

        public void evliPersonel()
        {
            baglanti.Open();
            SqlCommand komut2 = new SqlCommand("select count(*) from per_table where per_durum=1",baglanti);
            SqlDataReader d2 = komut2.ExecuteReader();
            while (d2.Read())
            {
                lbl_evlipersonel.Text = d2[0].ToString();
            }
            baglanti.Close();
        }

        public void bekarPersonel()
        {
            baglanti.Open();
            SqlCommand komut3 = new SqlCommand("select count(*) from per_table where per_durum=0",baglanti);
            SqlDataReader d3 = komut3.ExecuteReader();
            while (d3.Read())
            {
                lbl_bekarpersonel.Text = d3[0].ToString();
            }
            baglanti.Close();
        }

        public void sehirSayisi()
        {
            baglanti.Open();
            SqlCommand komut_sehir = new SqlCommand("select count(distinct(per_sehir)) from per_table",baglanti);
            SqlDataReader sehir_data = komut_sehir.ExecuteReader();
            while (sehir_data.Read())
            {
                lbl_sehir.Text = sehir_data[0].ToString();
            }
            baglanti.Close();
        }

        public void toplamMaas()
        {
            baglanti.Open();
            SqlCommand komutMaas = new SqlCommand("select sum(per_maas) from per_table",baglanti);
            SqlDataReader maas_data = komutMaas.ExecuteReader();
            while (maas_data.Read())
            {
                lbl_toplammaas.Text = maas_data[0].ToString();
            }
            baglanti.Close();
        }

        public void ortalamaMaas()
        {
            baglanti.Open();
            SqlCommand ortalamaMaas_komut = new SqlCommand("select avg(per_maas) from per_table",baglanti);
            SqlDataReader data_ortMaas = ortalamaMaas_komut.ExecuteReader();
            while (data_ortMaas.Read())
            {
                lbl_ortalamamaas.Text = data_ortMaas[0].ToString();
            }
            baglanti.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
        }
    }
}
