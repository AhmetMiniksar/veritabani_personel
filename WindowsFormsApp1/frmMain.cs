﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace WindowsFormsApp1
{
    public partial class frmMain : Form
    {
        public frmMain()
        {
            InitializeComponent();
        }

        private void button_listele_Click(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'personelVeriTabaniDataSet.per_table' table. You can move, or remove it, as needed.
            this.per_tableTableAdapter.Fill(this.personelVeriTabaniDataSet.per_table);
        }

        SqlConnection baglanti = new SqlConnection("Data Source=AHMETM91;Initial Catalog=PersonelVeriTabani;Integrated Security=True");
        private void button_kaydet_Click(object sender, EventArgs e)
        {
            baglanti.Open();
            SqlCommand komut = new SqlCommand("insert into per_table (per_ad,per_soyad,per_sehir,per_maas,per_meslek,per_durum) values (@p1,@p2,@p3,@p4,@p5,@p6)",baglanti);
            komut.Parameters.AddWithValue("@p1",text_perad.Text);
            komut.Parameters.AddWithValue("@p2",text_persoyad.Text);
            komut.Parameters.AddWithValue("@p3",combo_persehir.Text);
            komut.Parameters.AddWithValue("@p4",masked_maas.Text);
            komut.Parameters.AddWithValue("@p5",text_permeslek.Text);
            komut.Parameters.AddWithValue("@p6", lbl_kontrol.Text);
            komut.ExecuteNonQuery();
            baglanti.Close();
            MessageBox.Show("Personel Eklendi !", "durum", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void radio_evli_CheckedChanged(object sender, EventArgs e)
        {
            lbl_kontrol.Text = "true";
        }

        private void radio_bekar_CheckedChanged(object sender, EventArgs e)
        {
            lbl_kontrol.Text = "false";
        }
       
        public void temizle()
        {
            text_perad.Text = "";
            text_persoyad.Text = "";
            combo_persehir.Text = "";
            masked_maas.Text = "";
            text_permeslek.Text = "";
            radio_evli.Checked = false;
            radio_bekar.Checked = false;
            text_perad.Focus();
        }

        private void button_temizlik_Click(object sender, EventArgs e)
        {
            temizle();
        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            int secilen = dataGridView1.SelectedCells[0].RowIndex;
            text_perid.Text = dataGridView1.Rows[secilen].Cells[0].Value.ToString();
            text_perad.Text = dataGridView1.Rows[secilen].Cells[1].Value.ToString();
            text_persoyad.Text = dataGridView1.Rows[secilen].Cells[2].Value.ToString();
            combo_persehir.Text = dataGridView1.Rows[secilen].Cells[3].Value.ToString();
            masked_maas.Text = dataGridView1.Rows[secilen].Cells[4].Value.ToString();
            lbl_kontrol.Text = dataGridView1.Rows[secilen].Cells[5].Value.ToString();
            text_permeslek.Text = dataGridView1.Rows[secilen].Cells[6].Value.ToString();
        }

        private void lbl_kontrol_TextChanged(object sender, EventArgs e)
        {
            if (lbl_kontrol.Text == "True")
            {
                radio_evli.Checked = true;
                radio_bekar.Checked = false;
            }
            else if (lbl_kontrol.Text == "False")
            {
                radio_bekar.Checked = true;
                radio_evli.Checked = false;
            }
        }

        private void button_sil_Click(object sender, EventArgs e)
        {
            baglanti.Open();

            SqlCommand komut_sil = new SqlCommand("delete from per_table where per_id=@k1",baglanti);
            komut_sil.Parameters.AddWithValue("@k1",text_perid.Text);
            MessageBox.Show("Personel Silindi !", "durum", MessageBoxButtons.OK, MessageBoxIcon.Information);
            komut_sil.ExecuteNonQuery();
            baglanti.Close();
        }

        private void button_guncelle_Click(object sender, EventArgs e)
        {
            baglanti.Open();
            SqlCommand komut_guncelle = new SqlCommand("update per_table set per_ad=@a1,per_soyad=@a2,per_sehir=@a3,per_maas=@a4,per_durum=@a5,per_meslek=@a6 where per_id=@a7", baglanti);
            komut_guncelle.Parameters.AddWithValue("@a1",text_perad.Text);
            komut_guncelle.Parameters.AddWithValue("@a2",text_persoyad.Text);
            komut_guncelle.Parameters.AddWithValue("@a3",combo_persehir.Text);
            komut_guncelle.Parameters.AddWithValue("@a4",masked_maas.Text);
            komut_guncelle.Parameters.AddWithValue("@a5",lbl_kontrol.Text);
            komut_guncelle.Parameters.AddWithValue("@a6", text_permeslek.Text);
            komut_guncelle.Parameters.AddWithValue("@a7",text_perid.Text);
            komut_guncelle.ExecuteNonQuery();
            baglanti.Close();
            MessageBox.Show("Personel Güncellendi !", "durum", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void button_istatistik_Click(object sender, EventArgs e)
        {
            frmistatistik istatistik = new frmistatistik();
            istatistik.Show();
        }

        private void button_grafikler_Click(object sender, EventArgs e)
        {
            frmGrafikler grafik = new frmGrafikler();
            grafik.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            frmRaporlama rapor = new frmRaporlama();
            rapor.Show();
        }
    }
}
