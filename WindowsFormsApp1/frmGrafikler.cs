﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace WindowsFormsApp1
{
    public partial class frmGrafikler : Form
    {
        public frmGrafikler()
        {
            InitializeComponent();
        }

        SqlConnection baglanti = new SqlConnection("Data Source=AHMETM91;Initial Catalog=PersonelVeriTabani;Integrated Security=True");
        private void frmGrafikler_Load(object sender, EventArgs e)
        {
            sehirGrafik();
            meslekMaasGrafik();
        }

        public void sehirGrafik()
        {
            baglanti.Open();
            SqlCommand komut_graph = new SqlCommand("select per_sehir,count(*) from per_table group by per_sehir", baglanti);
            SqlDataReader data_graph = komut_graph.ExecuteReader();
            while (data_graph.Read())
            {
                chart1.Series["Sehirler"].Points.AddXY(data_graph[0], data_graph[1]);
            }
            baglanti.Close();
        }

        public void meslekMaasGrafik()
        {
            baglanti.Open();
            SqlCommand komut_graph = new SqlCommand("select per_sehir,avg(per_maas) from per_table group by per_sehir",baglanti);
            SqlDataReader data_graph = komut_graph.ExecuteReader();
            while (data_graph.Read())
            {
                chart2.Series["Meslek - Maas"].Points.AddXY(data_graph[0],data_graph[1]);
            }
            baglanti.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
        }
    }
}
