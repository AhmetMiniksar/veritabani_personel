﻿namespace WindowsFormsApp1
{
    partial class frmistatistik
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lbl_toplampersonel = new System.Windows.Forms.Label();
            this.lbl_evlipersonel = new System.Windows.Forms.Label();
            this.lbl_bekarpersonel = new System.Windows.Forms.Label();
            this.lbl_sehir = new System.Windows.Forms.Label();
            this.lbl_toplammaas = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.lbl_ortalamamaas = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(194)))), ((int)(((byte)(157)))), ((int)(((byte)(52)))));
            this.label1.Location = new System.Drawing.Point(36, 41);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(287, 29);
            this.label1.TabIndex = 0;
            this.label1.Text = "Toplam Personel Sayısı : ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(194)))), ((int)(((byte)(157)))), ((int)(((byte)(52)))));
            this.label2.Location = new System.Drawing.Point(80, 79);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(243, 29);
            this.label2.TabIndex = 1;
            this.label2.Text = "Evli Personel Sayısı : ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(194)))), ((int)(((byte)(157)))), ((int)(((byte)(52)))));
            this.label3.Location = new System.Drawing.Point(56, 117);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(267, 29);
            this.label3.TabIndex = 2;
            this.label3.Text = "Bekar Personel Sayısı : ";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(194)))), ((int)(((byte)(157)))), ((int)(((byte)(52)))));
            this.label4.Location = new System.Drawing.Point(165, 155);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(158, 29);
            this.label4.TabIndex = 3;
            this.label4.Text = "Şehir Sayısı : ";
            // 
            // lbl_toplampersonel
            // 
            this.lbl_toplampersonel.AutoSize = true;
            this.lbl_toplampersonel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl_toplampersonel.ForeColor = System.Drawing.SystemColors.InactiveCaption;
            this.lbl_toplampersonel.Location = new System.Drawing.Point(336, 41);
            this.lbl_toplampersonel.Name = "lbl_toplampersonel";
            this.lbl_toplampersonel.Size = new System.Drawing.Size(27, 29);
            this.lbl_toplampersonel.TabIndex = 4;
            this.lbl_toplampersonel.Text = "0";
            // 
            // lbl_evlipersonel
            // 
            this.lbl_evlipersonel.AutoSize = true;
            this.lbl_evlipersonel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl_evlipersonel.ForeColor = System.Drawing.SystemColors.InactiveCaption;
            this.lbl_evlipersonel.Location = new System.Drawing.Point(336, 79);
            this.lbl_evlipersonel.Name = "lbl_evlipersonel";
            this.lbl_evlipersonel.Size = new System.Drawing.Size(27, 29);
            this.lbl_evlipersonel.TabIndex = 5;
            this.lbl_evlipersonel.Text = "0";
            // 
            // lbl_bekarpersonel
            // 
            this.lbl_bekarpersonel.AutoSize = true;
            this.lbl_bekarpersonel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl_bekarpersonel.ForeColor = System.Drawing.SystemColors.InactiveCaption;
            this.lbl_bekarpersonel.Location = new System.Drawing.Point(336, 117);
            this.lbl_bekarpersonel.Name = "lbl_bekarpersonel";
            this.lbl_bekarpersonel.Size = new System.Drawing.Size(27, 29);
            this.lbl_bekarpersonel.TabIndex = 6;
            this.lbl_bekarpersonel.Text = "0";
            // 
            // lbl_sehir
            // 
            this.lbl_sehir.AutoSize = true;
            this.lbl_sehir.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl_sehir.ForeColor = System.Drawing.SystemColors.InactiveCaption;
            this.lbl_sehir.Location = new System.Drawing.Point(336, 155);
            this.lbl_sehir.Name = "lbl_sehir";
            this.lbl_sehir.Size = new System.Drawing.Size(27, 29);
            this.lbl_sehir.TabIndex = 7;
            this.lbl_sehir.Text = "0";
            // 
            // lbl_toplammaas
            // 
            this.lbl_toplammaas.AutoSize = true;
            this.lbl_toplammaas.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl_toplammaas.ForeColor = System.Drawing.SystemColors.InactiveCaption;
            this.lbl_toplammaas.Location = new System.Drawing.Point(336, 192);
            this.lbl_toplammaas.Name = "lbl_toplammaas";
            this.lbl_toplammaas.Size = new System.Drawing.Size(27, 29);
            this.lbl_toplammaas.TabIndex = 9;
            this.lbl_toplammaas.Text = "0";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(194)))), ((int)(((byte)(157)))), ((int)(((byte)(52)))));
            this.label10.Location = new System.Drawing.Point(145, 192);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(178, 29);
            this.label10.TabIndex = 8;
            this.label10.Text = "Toplam Maaş : ";
            // 
            // lbl_ortalamamaas
            // 
            this.lbl_ortalamamaas.AutoSize = true;
            this.lbl_ortalamamaas.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl_ortalamamaas.ForeColor = System.Drawing.SystemColors.InactiveCaption;
            this.lbl_ortalamamaas.Location = new System.Drawing.Point(336, 230);
            this.lbl_ortalamamaas.Name = "lbl_ortalamamaas";
            this.lbl_ortalamamaas.Size = new System.Drawing.Size(27, 29);
            this.lbl_ortalamamaas.TabIndex = 11;
            this.lbl_ortalamamaas.Text = "0";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(194)))), ((int)(((byte)(157)))), ((int)(((byte)(52)))));
            this.label12.Location = new System.Drawing.Point(130, 230);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(193, 29);
            this.label12.TabIndex = 10;
            this.label12.Text = "Ortalama Maaş : ";
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(176)))), ((int)(((byte)(13)))), ((int)(((byte)(27)))));
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(418, 12);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(36, 35);
            this.button1.TabIndex = 12;
            this.button1.Text = "X";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // frmistatistik
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(51)))), ((int)(((byte)(80)))));
            this.ClientSize = new System.Drawing.Size(466, 306);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.lbl_ortalamamaas);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.lbl_toplammaas);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.lbl_sehir);
            this.Controls.Add(this.lbl_bekarpersonel);
            this.Controls.Add(this.lbl_evlipersonel);
            this.Controls.Add(this.lbl_toplampersonel);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmistatistik";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmistatistik";
            this.Load += new System.EventHandler(this.frmistatistik_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lbl_toplampersonel;
        private System.Windows.Forms.Label lbl_evlipersonel;
        private System.Windows.Forms.Label lbl_bekarpersonel;
        private System.Windows.Forms.Label lbl_sehir;
        private System.Windows.Forms.Label lbl_toplammaas;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label lbl_ortalamamaas;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button button1;
    }
}